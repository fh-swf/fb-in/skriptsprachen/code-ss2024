from fastapi import FastAPI

app = FastAPI()

# Anfrage koennte z.B.
# http://localhost/val 
# sein -> soll Wert eines Zaehlers ausg.

counter = dict()

@app.get("/")
def startPage():
	return { "reply": "Try /val" }

@app.get("/create/{key}")
def createCounter(key):
	if key in counter:
		return { "status": "Exists",
		"id": key,
		"value": counter[key] }
	else:
		counter[key] = 0
		return { "status": "OK",
		"id": key,
		"value": counter[key] }

@app.get("/val/{key}")
def getValue(key):
	if key in counter:
		return { "status": "OK",
		"id": key,
		"value": counter[key] }
	else:
		return { "status": "ERR_Exist",
		"id": key }

@app.get("/inc/{key}")
def incValue(key):
	if key in counter:
		counter[key] += 1
		return { "status": "OK",
		"id": key,
		"value": counter[key] }
	else:
		return { "status": "ERR_Exist",
		"id": key }

@app.get("/dec/{key}")
def decValue(key):
	if key in counter:
		if counter[key] > 0:
			counter[key] -= 1
			return { "status": "OK",
			"id": key,
			"value": counter[key] }
		else:
			return { "status": "ERR_Zero",
			"id": key,
			"value": counter[key] }
	else:
		return { "status": "ERR_Exist",
		"id": key }

@app.post("/login")
def dumpData(user, password):
	if user=="abc" and password=="def":
		reply = dict()
		reply["user"] = user
		for key in counter:
			reply[key] = counter[key]
		return reply
	else:
		return { "status": "ERR_Login",
		"user": user }




