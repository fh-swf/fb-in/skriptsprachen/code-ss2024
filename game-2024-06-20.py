#!/usr/bin/env python3.11
# game.py

# basiert auf
# Clear Code, The ultimate introduction to Pygame
# https://www.youtube.com/watch?v=AY9MnQ4x3zk

import pygame
from sys import exit
from random import randint
import requests


WIDTH=800
HEIGHT=400

pygame.init()

# display surface erzeugen:
screen = pygame.display.set_mode((WIDTH,HEIGHT))

# Fenstertitel
pygame.display.set_caption("Bilder, Bilder!")

# timer initialisieren
clock = pygame.time.Clock()

# Schriften
font = pygame.font.Font(None, 50)
text1 = font.render("Bilder, Bilder!", False, "Green")


# Hintergrund
# Bild hat Abmessungen 960x200
background = pygame.image.load("img/scroller.gif")
background = pygame.transform.scale(background, (1920, 400))
bg_xpos = 0

# Player
player_surface = pygame.image.load("img/player_100.png")
player_rect = player_surface.get_rect(midbottom = (WIDTH//2, HEIGHT-50))
#player_posx = WIDTH/2
#player_posy = HEIGHT-150
player_walks_left = True
leben = 100

# Gegner
font2 = pygame.font.Font("Amstrad-CPC-Mode2.ttf", 48)
gegner = font2.render("S", False, "Red")
gegner_rect = gegner.get_rect(midbottom=(WIDTH//2, HEIGHT-50))

# Kollision?

# pygame.event.set_grab(True)   # Maus fangen

# Beliebig viele Surfaces erzeugen und auf Display 
# Surface positionieren

#s1 = pygame.Surface((70,50))
#s2 = s1.copy()
#s3 = s1.copy()
#s1.fill("Red")
#s2.fill("Green")
#s3.fill("Blue")

SCROLL_SPEED = 2

while True:
	# Hat es ein Event gegeben?
	# Wenn ja, darauf reagieren
	for ev in pygame.event.get():
		if ev.type == pygame.QUIT:
			pygame.quit()
			#print ("Ende, weil Fenster geschlossen")
			exit()
		if ev.type == pygame.KEYDOWN:
			if ev.key == pygame.K_q:
				pygame.quit()
				#print ("Ende, weil Tastendruck")
				exit()
			# Spieler-Steuerung
			if ev.key == pygame.K_RIGHT:
				player_rect.left += 40
				if player_walks_left:
					player_walks_left = False
					player_surface = pygame.transform.flip(player_surface,True,False)
			if ev.key == pygame.K_LEFT:
				if player_rect.left >= 40: 
					player_rect.left -= 40
				if not player_walks_left:
					player_walks_left = True
					player_surface = pygame.transform.flip(player_surface,True,False)
			if ev.key == pygame.K_j:
				player_rect.left += 300
				if player_walks_left:
					player_walks_left = False
					player_surface = pygame.transform.flip(player_surface,True,False)

	# Anzeige des Bildschirms
	screen.fill("Blue")
	screen.blit(background,(bg_xpos,0))
	screen.blit(background,(bg_xpos+1920,0))
	bg_xpos -= SCROLL_SPEED
	if bg_xpos < -1920: bg_xpos += 1920

	# Player
	screen.blit(player_surface,player_rect)
	if player_rect.left > 0: player_rect.left -= SCROLL_SPEED
	#screen.blit(s2, (randint(0,WIDTH-70),randint(0,HEIGHT-50)))
	#screen.blit(s3, (randint(0,WIDTH-70),randint(0,HEIGHT-50)))

	# Gegner
	screen.blit(gegner,gegner_rect)

	# Kollision
	if player_rect.colliderect(gegner_rect):
		# print ("KOLLISION", end=" ")
		leben -= 1

	# Leben anzeigen
	t_leben = font.render(str(leben), False, "White")
	screen.blit(t_leben, (WIDTH//2, 10))

	pygame.display.update()
	# ggfs. warten
	clock.tick(60)

# hinter der event loop


