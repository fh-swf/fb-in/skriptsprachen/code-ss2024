#!/usr/bin/env python3.11
# game.py

# basiert auf
# Clear Code, The ultimate introduction to Pygame
# https://www.youtube.com/watch?v=AY9MnQ4x3zk

import pygame
from sys import exit
from random import randint
import requests


WIDTH=800
HEIGHT=400

pygame.init()

# display surface erzeugen:
screen = pygame.display.set_mode((WIDTH,HEIGHT))

# Fenstertitel
pygame.display.set_caption("Bilder, Bilder!")

# timer initialisieren
clock = pygame.time.Clock()

# Schriften
font = pygame.font.Font(None, 50)
text1 = font.render("Bilder, Bilder!", False, "Green")


# Hintergrund
# Bild hat Abmessungen 960x200
background = pygame.image.load("assets/scroller.gif")
background = pygame.transform.scale(background, (1920, 400))
bg_xpos = 0

# Player
player_surface = pygame.image.load("assets/player_100.png")
player_rect = player_surface.get_rect(midbottom = (WIDTH//2, HEIGHT-50))
#player_posx = WIDTH/2
#player_posy = HEIGHT-150
player_walks_left = True
player_gravity = 0
leben = 100

# Gegner
font2 = pygame.font.Font("assets/Amstrad-CPC-Mode2.ttf", 48)
gegner=[]
gegner.append (font2.render("S", False, "Red"))
gegner.append (font2.render("W", False, "Green"))
gegner.append (font2.render("F", False, "Yellow"))
gegner_rect=[]
for i in [0,1,2]:
	gegner_rect.append (gegner[i].get_rect(midbottom=(i*WIDTH//3+50, i*50)))

# Laufschrift
font3 = pygame.font.Font("assets/Amstrad-CPC-Mode2.ttf", 24)
text2 = font3.render("Left,Right,J=Jump,Up,Q=Quit", False, "Yellow")
text2_pos = WIDTH

# Animierte Gegner
anigegner_filenames = [ f"assets/anim/anim-a-{i}.png" for i in range(0,16) ]
anigegner_surfaces = [ pygame.image.load(f).convert_alpha() for f in anigegner_filenames ]
anigegner_surfaces = [ pygame.transform.scale(s,(90,90)) for s in anigegner_surfaces ]
anigegner_rect = anigegner_surfaces[0].get_rect(midbottom=(WIDTH,HEIGHT-55))
anigegner_index = 0
gegner_rect.append(anigegner_rect)

# Kollision?

# pygame.event.set_grab(True)   # Maus fangen

# Beliebig viele Surfaces erzeugen und auf Display 
# Surface positionieren

#s1 = pygame.Surface((70,50))
#s2 = s1.copy()
#s3 = s1.copy()
#s1.fill("Red")
#s2.fill("Green")
#s3.fill("Blue")

SCROLL_SPEED = 2

gameover = False
maus_pos = (0,0)

while True:
	# Hat es ein Event gegeben?
	# Wenn ja, darauf reagieren
	for ev in pygame.event.get():
		if ev.type == pygame.QUIT:
			pygame.quit()
			#print ("Ende, weil Fenster geschlossen")
			exit()
		if ev.type == pygame.MOUSEMOTION:
			# print ("DEBUG: Mauspos. = ", ev.pos)
			maus_pos = ev.pos
		if ev.type == pygame.KEYDOWN:
			if ev.key == pygame.K_q:
				pygame.quit()
				#print ("Ende, weil Tastendruck")
				exit()
			# Spieler-Steuerung
			if not gameover and ev.key == pygame.K_RIGHT:
				player_rect.left += 40
				if player_walks_left:
					player_walks_left = False
					player_surface = pygame.transform.flip(player_surface,True,False)
			if not gameover and ev.key == pygame.K_LEFT:
				if player_rect.left >= 40: 
					player_rect.left -= 40
				if not player_walks_left:
					player_walks_left = True
					player_surface = pygame.transform.flip(player_surface,True,False)
			if not gameover and ev.key == pygame.K_UP:
				player_rect.y -= 200
				player_gravity = 1

			if not gameover and ev.key == pygame.K_j:
				player_rect.left += 300
				if player_walks_left:
					player_walks_left = False
					player_surface = pygame.transform.flip(player_surface,True,False)

	# Anzeige des Bildschirms
	screen.fill("Blue")
	screen.blit(background,(bg_xpos,0))
	screen.blit(background,(bg_xpos+1920,0))
	if not gameover:
		bg_xpos -= SCROLL_SPEED
	if bg_xpos < -1920: bg_xpos += 1920

	# Laufschrift
	screen.blit(text2, (text2_pos,HEIGHT-40))
	text2_pos -= 3
	if text2_pos < -300: text2_pos = WIDTH

	# Player
	if player_rect.bottom == 350: player_gravity = 0

	screen.blit(player_surface,player_rect)
	if not gameover and player_rect.left > 0: player_rect.left -= SCROLL_SPEED
	if not gameover and player_rect.bottom < 350: 
		player_rect.bottom += int(player_gravity)
		player_gravity += 0.3
	#screen.blit(s2, (randint(0,WIDTH-70),randint(0,HEIGHT-50)))
	#screen.blit(s3, (randint(0,WIDTH-70),randint(0,HEIGHT-50)))

	# Gegner
	if not gameover:
		for i in [0,1,2]:
			gegner_rect[i].top += 1
			if gegner_rect[i].top > HEIGHT:
				gegner_rect[i].top = -48
	for i in [0,1,2]:
		screen.blit(gegner[i],gegner_rect[i])

	# Animierte Gegner
	if not gameover:
		anigegner_rect.x -= 3
		screen.blit(anigegner_surfaces[int(anigegner_index)],anigegner_rect)
		anigegner_index += 0.2
		if anigegner_index > 15: anigegner_index = 0
		if anigegner_rect.x < -80: anigegner_rect.x = WIDTH

	# Kollision
	# if not gameover and player_rect.colliderect(gegner_rect[0]):
	if not gameover and player_rect.collidelist(gegner_rect) >= 0:
		# print ("KOLLISION", end=" ")
		leben -= 1
		if leben == 0:
			gameover = True
	if player_rect.collidepoint(maus_pos):
		leben += 10
		gameover = False

	# Code um bei Kontakt mit bestimmter Phase des Gegners sofort
	# das Spiel zu beenden
	#SCHWERT_OBEN_INDEX = 5
	#if not gameover and player_rect.colliderect(anigegner_rect) and int(anigegner_index) == SCHWERT_OBEN_INDEX:
	#	leben = 0
	#	gameover = True

	# Leben anzeigen
	t_leben = font.render(str(leben), False, "White")
	screen.blit(t_leben, (WIDTH//2, 10))

	pygame.display.update()
	# ggfs. warten
	clock.tick(60)

# hinter der event loop


