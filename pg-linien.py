#!/usr/bin/env python3.11
# pg-linien.py
# Line Drawing, 2024-06-20

import pygame
from sys import exit
from random import randint
import requests

WIDTH=800
HEIGHT=400

pygame.init()

# display surface
screen = pygame.display.set_mode((WIDTH,HEIGHT))

pygame.display.set_caption("Linien, Linien...")
clock = pygame.time.Clock()

maus_pos = (0,0)

color = (255,255,255)  # weiss
col_counter = 0

def update_color():
    # return
    global color, col_counter
    col_counter += 1
    if col_counter > 30:
        col_counter = 0
    else:
        return
    index = randint(0,2)
    cols = list(color)
    cols[index] += 1
    if cols[index] > 255: cols[index]=0
    color = tuple(cols)


while True:
    for ev in pygame.event.get():
        if ev.type == pygame.QUIT:
            pygame.quit(); exit()
        if ev.type == pygame.KEYDOWN:
            if ev.key == pygame.K_q:
                pygame.quit(); exit()

        # Mauspositionen!!
        if ev.type == pygame.MOUSEMOTION:
            # print ("Maus: ", ev.pos)
            maus_pos = ev.pos   # aktualisiere maus_pos!

    # Hintergrundbild ist transparent; brauche Farbe
    screen.fill("Blue")

    # links, oben nach unten
    for y in range(0, HEIGHT, 20):
        update_color()
        pygame.draw.line(screen, color, (0,    y), maus_pos, 2)
    # unten, links nach rechts
    for x in range(0, WIDTH, 20):
        update_color()
        pygame.draw.line(screen, color, (x, HEIGHT), maus_pos, 2)
    # rechts, unten nach oben
    for y in range(HEIGHT, 0, -20):
        update_color()
        pygame.draw.line(screen, color, (WIDTH,y), maus_pos, 2)
    # oben, rechts nach links
    for x in range(WIDTH, 0, -20):
        update_color()
        pygame.draw.line(screen, color, (x, 0), maus_pos, 2)

    pygame.display.update()
    clock.tick(60)


